# VHDL List 3

## Requirements for opening project

- Vivado 2021.2

- Installed tcl scripts for git integration: https://github.com/barbedo/vivado-git

## How to open project using .tcl script

When opening the project after cloning it, do it by using `Tools -> Run Tcl Script...` and selecting the `vhdl-list-3.tcl`.

## How to open project if .tcl does not work

Create new project and just add files in project tree:

`RMB: Design Sources -> Add Sources -> Add Or Create Design Sources -> Add Directories -> choose /desing_sources/ directory.`

and:

`RMB: Simulation Sources -> Add Sources -> Add Or Create Simulation Sources -> Add Directories -> choose /test_benches/ directory.`

## How to commit changes

Follow instruction at https://github.com/barbedo/vivado-git

## Tasks & solutions

Click photo bellow to get the PDF.

[![Tasks](/doc/Tasks.png)](/doc/LISTA_3___FPGA_ENG.pdf)

### Task 1

![Task 1](/doc/Task1.png)

![Task 1 RTL schematic](/doc/Task1_RTL_SCHEMATIC.png)

![Task 1 Solution](/doc/Task1_Plot.png)

### Task 2

![Task2](/doc/Task2.png)

![Task 2 RTL schematic](/doc/Task2_RTL_SCHEMATIC.png)

![Task 2 Solution](/doc/Task2_Plot.png)

### Task 3

![Task3](/doc/Task3.png)

![Task3](/doc/Task3_Synthesis%20schematic%20for%206%20delays.png)

#### Plot for 6 delays

![Task3](/doc/Task3_Plot_6delays.png)

#### Plot for 12 delays

![Task3](/doc/Task3_Plot_12delays.png)

### Task 4

![Task4](/doc/Task4.png)

#### Frequency multiplier x4

![Frequency multiplier x4](/doc/Task4_Plot_Multiplyx4.png)

#### Frequency multiplier x8

![Frequency multiplier x4](/doc/Task4_Plot_Multiplyx8.png)

#### RTL Schematic of task 4

![Task4 schematic](/doc/Task4_RTL_Schematic.png)

#### Result - multiply x 2 and divide by 5

![Task4 result](/doc/Task4_Plot_x2div5.png)

### Task 5

![Task5](/doc/Task5.png)

## Author of solution

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel)

## Contribution

https://vhdl.lapinoo.net/testbench/