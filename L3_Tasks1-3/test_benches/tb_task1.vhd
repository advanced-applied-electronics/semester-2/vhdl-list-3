library ieee;
use ieee.std_logic_1164.all;

entity tb_task1 is
end tb_task1;

architecture tb of tb_task1 is

    component task1
        port (CLK         : in std_logic;
              CLK_DOUBLED : out std_logic);
    end component;

    signal CLK         : std_logic;
    signal CLK_DOUBLED : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : task1
    port map (CLK         => CLK,
              CLK_DOUBLED => CLK_DOUBLED);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- EDIT Add stimuli here
        wait for 1000 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;