-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 15.1.2023 12:12:18 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_task3_generic_doubler is
end tb_task3_generic_doubler;

architecture tb of tb_task3_generic_doubler is

    component task3_generic_doubler        
        port (CLK_IN  : in std_logic;
              CLK_OUT : out std_logic);
    end component;

    signal CLK_IN  : std_logic := '0';
    signal CLK_OUT : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbSimEnded : std_logic := '0';

begin

    dut : task3_generic_doubler    
    port map (CLK_IN  => CLK_IN,
              CLK_OUT => CLK_OUT);

    -- Clock generation
    CLK_IN <= not CLK_IN after TbPeriod/2 when TbSimEnded /= '1' else '0';

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;