----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2023 14:09:54
-- Design Name: 
-- Module Name: task1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity task1 is
    Port ( CLK : in STD_LOGIC;
           CLK_DOUBLED : out STD_LOGIC);
end task1;

architecture Behavioral of task1 is

signal not1: std_logic;
signal not2: std_logic;
signal not3: std_logic;
signal not4: std_logic;

attribute dont_touch : string;
attribute dont_touch of not1: signal is "true";
attribute dont_touch of not2: signal is "true";
attribute dont_touch of not3: signal is "true";
attribute dont_touch of not4: signal is "true";

begin
    not1 <= not(CLK);
    not2 <= not(not1);
    not3 <= not(not2);
    not4 <= not(not3);
    
    CLK_DOUBLED <= CLK XOR not4;

end Behavioral;
