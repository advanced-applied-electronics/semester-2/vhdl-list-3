library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity task2_generate_doubler is
    Port ( CLK_IN : in STD_LOGIC;
           CLK_OUT : out STD_LOGIC);
end task2_generate_doubler;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity my_not_gate is
    Port ( CLK_IN : in STD_LOGIC;
           CLK_OUT : out STD_LOGIC);
end my_not_gate;

architecture Behavioral of my_not_gate is

begin

process(CLK_IN)
begin
    CLK_OUT <= not CLK_IN;
end process;  
end Behavioral;

architecture Behavioral of task2_generate_doubler is
    constant i : positive := 6;
    signal not_vector : std_logic_vector(i downto 0) := ( others => '0');
    attribute dont_touch : string;
    attribute dont_touch of not_vector : signal is "true";
    
    component my_not_gate is
        Port ( CLK_IN : in STD_LOGIC;
               CLK_OUT : out STD_LOGIC);
    end component;         
               
    begin
    
    process(CLK_IN)
        begin
            not_vector(0) <= CLK_IN;
            CLK_OUT <= not_vector(i) xor CLK_IN;
        end process;  
        
    connect_not_vector_elements: for j in 0 to (i - 1) generate
        u: my_not_gate 
        port map(CLK_IN => not_vector(j), CLK_OUT => not_vector(j+1));
        end generate connect_not_vector_elements;
        
end Behavioral;