-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 15.1.2023 23:41:47 UTC

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity tb_task4 is
end tb_task4;

architecture tb of tb_task4 is

    component task4
        port (CLK_IN  : in std_logic;
              CLK_OUT : out std_logic;
              RST     : in std_logic;
              SET_MUL : in std_logic;
              MUL_SEL : in std_logic_vector (1 downto 0);
              DIV_MOD : in std_logic_vector (5 downto 0);
              CLK_MULT_OUT : out std_logic);
    end component;

    signal CLK_IN  : std_logic;
    signal CLK_OUT : std_logic;
    signal RST     : std_logic;
    signal SET_MUL : std_logic;
    signal MUL_SEL : std_logic_vector (1 downto 0);
    signal DIV_MOD : std_logic_vector (5 downto 0);
    signal CLK_MULT_OUT : std_logic;

    constant TbPeriod : time := 40 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : task4
    port map (CLK_IN  => CLK_IN,
              CLK_OUT => CLK_OUT,
              RST     => RST,
              SET_MUL => SET_MUL,
              MUL_SEL => MUL_SEL,
              DIV_MOD => DIV_MOD,
              CLK_MULT_OUT => CLK_MULT_OUT);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK_IN is really your main clock signal
    CLK_IN <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        SET_MUL <= '0';
        MUL_SEL <= (others => '0');
        DIV_MOD <= (others => '0');

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST <= '1';
        
        wait for 100 ns;
        RST <= '0';
        
        wait for 100 ns;
        DIV_MOD <= std_logic_vector(to_unsigned(5, DIV_MOD'length));
        
        wait for 100ns;
        -- Select frequency multiplication ratio 
        -- "00" = freq * 1
        -- "01" = freq * 2
        -- "10" = freq * 4
        -- "11" = freq * 8
        MUL_SEL <= "01";
        
        wait for 100ns;
        
        SET_MUL <= '1';
        wait for 100ns;
        SET_MUL <= '0';      
        

        -- EDIT Add stimuli here
        wait for 800 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;
