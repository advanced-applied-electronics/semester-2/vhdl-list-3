----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.01.2023 16:50:23
-- Design Name: 
-- Module Name: freq_multiplier - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity freq_multiplier is
    generic ( number_of_delays: positive:= 24;
              doubling_amount: positive := 2 );
    Port ( CLK_IN : in STD_LOGIC;
           CLK_OUT : out STD_LOGIC);
end freq_multiplier;

architecture Behavioral of freq_multiplier is    
    signal delays_vector : std_logic_vector(doubling_amount downto 0) := ( others => '0');
    --constant max_delay: positive := 24;
    -- attribute dont_touch : string;
    -- attribute dont_touch of delays_vector : signal is "true";
        
    component generic_doubler
        generic ( nr_delays: positive:= number_of_delays);         
        port (CLK_IN  : in std_logic;
              CLK_OUT : out std_logic);
    end component;    
begin
    process(CLK_IN)
    begin
        delays_vector(0) <= CLK_IN;
        CLK_OUT <= delays_vector(doubling_amount);
    end process;
    
    connect_delays_vector_elements: for j in 0 to (doubling_amount - 1) generate
        u: generic_doubler
        -- generic map ( nr_delays => number_of_delays ) 
        generic map ( nr_delays => number_of_delays / (j+1) ) 
        port map( CLK_IN => delays_vector(j), CLK_OUT => delays_vector(j+1));
        end generate connect_delays_vector_elements;
end Behavioral;
